# queenb

queenb is for automating configuration and launching of
services, of which requires dozens of manual processes.
With this script, you no longer need to elaborate on the
steps for testing, but all you need to do is type one command
and wait.

## Requirement

```
Ruby >= 2.3.x
docker >= 18.03.x
docker-compose >= 1.21.x
nodejs >= 6.5.x
npm >= 3.5.x
```

## Installation

type the following command:
```
$ ./installer
```

## Usage

### launch polyswarmd and its contract

First, you need to login to the gitlab's docker registry.
```
$ queenb login
```

Then, start the polyswarmd:
```
$ queenb start
```

### post bounty

```
$  queenb bounty artifacts/clean_1.exe 
```

## Author

Kei Sugano <ks@polyswarm.io>
