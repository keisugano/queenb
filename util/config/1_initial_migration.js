console.log("init Web3, NectarToken, Migration ...")
const Web3        = require('web3');
const NectarToken = artifacts.require('NectarToken');
var Migrations    = artifacts.require("./Migrations.sol");
var web3 = new Web3();
console.log("set Provider...")
web3.setProvider(new web3.providers.HttpProvider('http://localhost:8545'));

web3.personal.unlockAccount('0x5249CC16cA2577BC0C4fEb3519439FA4748eBD47', 'moomin')

// NectarToken.at(NectarToken.address).mint(web3.eth.coinbase, '10000000000000000000000')
// NectarToken.at(NectarToken.address).enableTransfers()
module.exports = function(deployer) {
  deployer.deploy(Migrations);
};
