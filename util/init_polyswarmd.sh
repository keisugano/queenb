#!/usr/bin/env bash  

ETC="$HOME/.queenb/util"
TMP="$ETC/tmp"   
DOCKER_CONFIG="$ETC/config"

function say {
  echo "[$1] ..."
}

sudo chown -R $whoami:$whoami $HOME/.queenb

say "cleaning doneflag ..."
if [ -f $TMP/.doneflag ]; then
  rm $TMP/.doneflag
fi

say "start docker machine ..."
OS=$(uname -a)
if [[ $OS =~ ^Darwin ]]; then
  say "Starting OSX"
  sudo xcode-select --switch /Library/Developer/CommandLineTools
  docker-machine rm default 
  docker-machine create --driver virtualbox default
  docker-machine start
  say "Eval"
  eval "$(docker-machine env default)"
else
  say "Starting Linux"
  sudo service docker restart
fi

say "cleaning docker processes ..."
PROCESSES=$(pgrep docker-compose)
for p in $PROCESSES; do
    echo "killing $p"
    sudo kill $p
done

DATADIR="$HOME/.ethereum/priv_testnet"
DATADIR_BACKUP="$HOME/.ethereum/priv_testnet_bk"

say "removing polyswarmd"
cd $TMP
if [ -d polyswarmd ]; then
    sudo rm -rf polyswarmd
fi

say "cloning polyswarmd..."
sudo chown -R $whoami:$whoami .
sudo git clone https://github.com/polyswarm/polyswarmd.git
say "cd polyswarmd"
cd polyswarmd
sudo git pull
say "copying priv testnet"
sudo cp $DOCKER_CONFIG/docker-compose-priv-testnet.yml docker
echo "copying polyswarmd.py"
sudo cp $DOCKER_CONFIG/polyswarmd.py .
sudo cp $DOCKER_CONFIG/mint_tokens.sh .

say "cloning truffle..."
if [ -d polyswarm-truffle ]; then
    rm -rf polyswarm-truffle
fi

sudo git clone https://github.com/polyswarm/polyswarm-contracts.git

say "copying truffle"
sudo rm -rf truffle
sudo cp -r polyswarm-contracts/ truffle
cd ..

say "copying priv_testnet: $DATADIR"
pwd
sudo cp -r $DOCKER_CONFIG/priv_testnet/* $DATADIR_BACKUP
sudo cp -r $DOCKER_CONFIG/priv_testnet/* ~/.ethereum

say "cp $DOCKER_CONFIG/truffle-config.js  polyswarmd/truffle"
sudo cp $DOCKER_CONFIG/truffle-config.js  polyswarmd/truffle

say "cd truffle"
cd polyswarmd/truffle

say "downloading node modules from LFS ..."
sudo git clone https://gitlab.com/keisugano/lfs-queenb.git
sudo mv lfs-queenb/node_modules .
say "installing truffle ..."
sudo npm config rm proxy
sudo npm config rm https-proxy
sudo npm install -g truffle

say "compiling truffle ..."
sudo truffle compile

cd ../
say "Launching Geth"
sudo docker-compose -f docker/docker-compose-priv-testnet.yml up geth  >/dev/null &

say "Sleep for 3 seconds"
sleep 3
pwd
$ETC/port_listen.sh
cd ..

say "Second phase"
sudo $ETC/step2.sh
