#!/usr/bin/env bash

OS=$(uname -a)  
if [[ $OS =~ ^Darwin ]]; then
    lsof -i | grep LISTEN
else
    netstat -ntlp | grep LISTEN
fi
