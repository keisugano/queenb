#!/usr/bin/env ruby

def say(txt)
  puts "[#{txt}] ..."
end

def dev_null(cmd)
  say "#{cmd} >/dev/null &"
  system "#{cmd} >/dev/null &"
end

ETC="#{Dir.home}/.queenb/util"
TMP="#{ETC}/tmp"

say "sudo truffle migrate reset"
res = `cd #{TMP}/polyswarmd/truffle; sudo truffle migrate --reset`

nectar_token    = /NectarToken\:.+/.match(res).to_s.gsub("NectarToken: ", "")
bounty_registry = /BountyRegistry\:.+/.match(res).to_s.gsub("BountyRegistry: ", "")

config = "NECTAR_TOKEN_ADDRESS = '#{nectar_token}'\n" + 
         "BOUNTY_REGISTRY_ADDRESS = '#{bounty_registry}'"

File.open("#{TMP}/polyswarmd/polyswarmd.docker.cfg", "w") do |f|
  f.puts config
end
File.open("#{TMP}/polyswarmd/polyswarmd.cfg", "w") do |f|
  f.puts config
end

say "build"
system "cd #{TMP}/polyswarmd; sudo docker build -t polyswarm/polyswarmd -f docker/Dockerfile ."

say "Launch GETH/IPFS/POLYSWARMD"
dev_null "cd #{TMP}/polyswarmd; sudo docker-compose -f docker/docker-compose-priv-testnet.yml up"

say "Waiting..."
sleep 10
system "#{ETC}/port_listen.sh"
system "touch #{TMP}/.doneflag"
